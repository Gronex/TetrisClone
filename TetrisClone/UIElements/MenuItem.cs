using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace TetrisClone.UIElements
{
    public class MenuItem : Transformable, Drawable
    {
        private bool _selected;
        public Text Text { get; }
        public Action SelectionAction { get; }

        public bool Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                Text.Color = value ? Color.Blue : Color.White;
            }
        }

        public MenuItem(string text, Action action)
        {
            Text = new Text(text, new Font("C:/Windows/Fonts/Arial.ttf"));
            SelectionAction = action;
            Selected = false;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;

            states.Transform.Translate(Position);
            
            target.Draw(Text, states);
        }
    }
}
