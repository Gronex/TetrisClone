using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace TetrisClone.UIElements
{
    public class Menu : Transformable, Drawable
    {
        private readonly LinkedList<MenuItem> _elements;
        private LinkedListNode<MenuItem> _selected;

        public Menu(Vector2f position)
        {
            Position = position;
            _elements = new LinkedList<MenuItem>();
        }

        public void AddItem(MenuItem item)
        {
            var itemWidth = item.Text.GetGlobalBounds().Width;
            if (_elements.Any())
            {
                var lastItem = _elements.Last();
                item.Position = new Vector2f(0, lastItem.Position.Y + lastItem.Text.GetGlobalBounds().Height);
            }
            item.Origin = new Vector2f(itemWidth/2, 0);
            _elements.AddLast(item);
            if(_elements.Count == 1) SelectUp();
        }

        public void SelectUp(bool wrap = false)
        {
            if(!_elements.Any()) throw new InvalidOperationException("No menu items");
            if (_selected == null)
                _selected = _elements.First;
            else
            {
                _selected.Value.Selected = false;
                _selected = _selected.Previous ?? (wrap ? _elements.Last : _selected);
            }
            _selected.Value.Selected = true;
        }

        public void SelectDown(bool wrap = false)
        {
            if (!_elements.Any()) throw new InvalidOperationException("No menu items");
            if (_selected == null)
                _selected = _elements.First;
            else
            {
                _selected.Value.Selected = false;
                _selected = _selected.Next ?? (wrap ? _elements.First : _selected);
            }
            _selected.Value.Selected = true;
        }

        public void ExecuteSelected()
        {
            _selected?.Value.SelectionAction();
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;

            foreach (var item in _elements)
            {
                target.Draw(item, states);
            }
        }
    }
}
