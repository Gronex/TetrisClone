using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using TetrisClone.Utils;

namespace TetrisClone
{
    public class StateManager
    {
        public MaxStack<State> StateStack { get; set; }

        public StateManager(int stateCap = 10)
        {
            StateStack = new MaxStack<State>(stateCap);
        }

        public void ChangeState(State newState, RenderWindow window)
        {
            StateStack.Push(newState);
            newState.Initialize(window);
        }

        public void EarlierState()
        {
            var state = StateStack.Pop();
            state.Dispose();
        }

        public State CurrentState => StateStack.Head;
    }
}
