using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using TetrisClone.Models;
using TetrisClone.States;
using TetrisClone.Utils;

namespace TetrisClone
{
    public class Program
    {
        private static void OnClose(object sender, EventArgs e)
        {
            // Close the window when OnClose event is received
            var window = (RenderWindow)sender;
            window.Close();
        }

        private static void Main(string[] args)
        {
            // Create the main window
            var app = new RenderWindow(new VideoMode(800, 600), "Tetris", Styles.Close);
            app.Closed += OnClose;
            

            var stateManager = new StateManager();
            var initialState = new MainMenu(stateManager);
            
            stateManager.ChangeState(initialState, app);
            // Start the game loop
            while (app.IsOpen)
            {
                // Process events
                app.DispatchEvents();

                stateManager.CurrentState.Update();

                stateManager.CurrentState.Draw(app);
            }
        }
    }
}
