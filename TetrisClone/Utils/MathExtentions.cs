using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TetrisClone.Utils
{
    public static class MathExtentions
    {
        public static double ToRadian(this double degree)
        {
            return (Math.PI/180)*degree;
        }

        public static double ToRadian(this int degree)
        {
            return ToRadian((double)degree);
        }
    }
}
