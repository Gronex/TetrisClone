using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TetrisClone.Utils
{
    public class MaxStack<T> : ICollection<T>
    {
        private readonly LinkedList<T> _stack;
        public readonly int Capacity;

        public MaxStack(int capacity)
        {
            _stack = new LinkedList<T>();
            Capacity = capacity;
        }

        public MaxStack(Stack<T> stack, int capacity)
        {
            _stack = new LinkedList<T>();
            Capacity = capacity;
            while (stack.Any() && stack.Count < capacity)
            {
                this.Push(stack.Pop());
            }
        }

        public void Push(T item)
        {
            if (_stack.Count >= Capacity)
                _stack.RemoveLast();
            _stack.AddFirst(item);
        }

        public T Pop()
        {
            var item = Head;
            _stack.RemoveFirst();
            return item;
        }

        public T Peek()
        {
            return Head;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _stack.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            _stack.AddFirst(item);
            if (_stack.Count > Capacity)
                _stack.RemoveLast();
        }

        public void Clear()
        {
            _stack.Clear();
        }

        public bool Contains(T item)
        {
            return _stack.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _stack.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return _stack.Remove(item);
        }
        
        public T Head
        {
            get
            {
                if(_stack.Any())
                    return _stack.First.Value;
                throw new InvalidOperationException("Stack is empty");
            }
        } 
        public int Count => _stack.Count;
        public bool IsReadOnly => true;
    }
}
