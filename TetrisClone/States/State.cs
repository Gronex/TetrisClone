using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;

namespace TetrisClone
{
    public abstract class State
    {
        protected readonly StateManager StateManager;
        protected Color ClearColor { get; set; }
        protected RenderWindow Window { get; private set; }

        protected State(StateManager stateManager)
        {
            StateManager = stateManager;
            ClearColor = new Color(101, 156, 239); //Cornflower blue
        }

        public virtual void Initialize(RenderWindow window)
        {
            Window = window;
        }

        public abstract void Update();

        public virtual void Dispose() {}

        public virtual void Pause() {}
        public virtual void Resume() {}

        public abstract void Draw(RenderWindow window);


    }
}
