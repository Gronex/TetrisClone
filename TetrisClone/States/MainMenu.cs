﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using TetrisClone.UIElements;

namespace TetrisClone.States
{
    public class MainMenu : State
    {
        private Menu _menu;

        public MainMenu(StateManager stateManager) : base(stateManager)
        {
        }

        public override void Initialize(RenderWindow window)
        {
            base.Initialize(window);
            _menu = new Menu(new Vector2f(window.Size.X/2.0f, 50));
            _menu.AddItem(new MenuItem("Play", () => StateManager.ChangeState(new Game(StateManager), window)));
            _menu.AddItem(new MenuItem("Options", () => Console.WriteLine("Not implemented")));
            _menu.AddItem(new MenuItem("Exit", window.Close));

            window.KeyPressed += OnKeyDown;
        }

        private void OnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.Code)
            {
                case Keyboard.Key.Up:
                    _menu.SelectUp(true);
                    break;
                case Keyboard.Key.Down:
                    _menu.SelectDown(true);
                    break;
                case Keyboard.Key.Return:
                    _menu.ExecuteSelected();
                    break;
                case Keyboard.Key.Escape:
                    if(StateManager.StateStack.Count > 1)
                        StateManager.EarlierState();
                    else
                        Window.Close();
                    break;
                default:
                    break;
            }
        }

        public override void Update()
        {
            
            //TODO: Handle something
        }

        public override void Dispose()
        {
            Window.KeyPressed -= OnKeyDown;
        }

        public override void Draw(RenderWindow window)
        {
            window.Clear(ClearColor);
            window.Draw(_menu);
            window.Display();
        }
    }
}
