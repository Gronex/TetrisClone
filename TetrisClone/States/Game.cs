using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using TetrisClone.Models;
using TetrisClone.States;

namespace TetrisClone
{
    public class Game : State
    {
        private const int BoardHeight = 20;
        private const int BoardWidth = 10;
        private const int TetraminoSideLength = 25;


        private View _gameView;
        private Stopwatch _timer;
        private Tetramino _selected;
        private GameBoard _gameBoard;
        private bool _pause;
        
        public Game(StateManager stateManager) : base(stateManager)
        {
        }

        public override void Initialize(RenderWindow window)
        {
            base.Initialize(window);
            _timer = new Stopwatch();

            _gameView = new View(window.DefaultView);
            _gameView.Move(new Vector2f(-_gameView.Size.X/2 + BoardWidth*TetraminoSideLength/2.0f, 0));

            _selected = _selected = Tetramino.RandomTetramino(TetraminoSideLength);
            _gameBoard = new GameBoard(BoardHeight, BoardWidth);
            
            window.KeyPressed += KeyDownEvent;
            
            _timer.Restart();
        }

        private void KeyDownEvent(object sender, KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.Code)
            {
                case Keyboard.Key.Left:
                    _selected.Move(new Vector2f(-1, 0));
                    if (CheckColition(_selected))
                        _selected.Move(new Vector2f(1, 0));
                    break;
                case Keyboard.Key.Right:
                    _selected.Move(new Vector2f(1, 0));
                    if (CheckColition(_selected))
                        _selected.Move(new Vector2f(-1, 0));
                    break;
                case Keyboard.Key.Down:
                    Down();
                    break;
                case Keyboard.Key.Up:
                    _selected.Rotate(90);
                    if(CheckColition(_selected))
                        _selected.Rotate(-90);
                    break;
                case Keyboard.Key.R:
                    _selected = Tetramino.RandomTetramino(TetraminoSideLength);
                    break;
                case Keyboard.Key.Escape:
                    StateManager.ChangeState(new MainMenu(StateManager), Window);
                    break;
            }
        }

        private void Down()
        {
            _selected.Move(new Vector2f(0, 1));
            if (!CheckColition(_selected)) return;
            _selected.Move(new Vector2f(0, -1));
            _gameBoard.Place(_selected);

            //TODO: Give score based on this
            var cleared = _gameBoard.ClearCheck();
            if(cleared > 0) Console.WriteLine($"{cleared} Cleared");

            _selected = Tetramino.RandomTetramino(TetraminoSideLength);

            if (CheckColition(_selected))
            {
                Dispose();
                Initialize(Window);
            }
        }

        private bool CheckColition(Tetramino tetramino)
        {
            return tetramino.Blocks
                .Select(b => b.RelativePosition)
                .Any(b => _gameBoard.IsTaken((int)b.X, (int)b.Y));
        }

        public override void Update()
        {
            if (_pause) return;
            if (_timer.Elapsed > new TimeSpan(0, 0, 1))
            {
                Down();

                _timer.Restart();
            }

        }

        public override void Dispose()
        {
            Window.KeyPressed -= KeyDownEvent;
            base.Dispose();
        }
        
        public override void Draw(RenderWindow window)
        {
            // Clear window
            window.Clear(ClearColor);
            
            window.SetView(_gameView);

            window.Draw(_gameBoard);
            window.Draw(_selected);

            // Update the window
            window.Display();
        }

    }
}
