using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace TetrisClone.Models
{
    public class Block : Transformable, Drawable
    {
        public Block(int sideLength, Color color, Vector2f coords = default(Vector2f))
        {
            SideLength = sideLength;
            Vertecies = new VertexArray(PrimitiveType.Quads, 4);
            Position = coords*sideLength;
            Color = color;
        }
        
        public Color Color { get; set; }

        public Vector2f RelativePosition => Position/SideLength;

        public int SideLength { get; set; }
        public VertexArray Vertecies { get; set; }
        
        public void Move(Vector2f direction)
        {
            Position = new Vector2f(Position.X + direction.X*SideLength, Position.Y + direction.Y*SideLength);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
            
            Vertecies[0] = new Vertex(new Vector2f(0,0), Color);
            Vertecies[3] = new Vertex(new Vector2f(0, SideLength), Color);
            Vertecies[2] = new Vertex(new Vector2f(SideLength, SideLength), Color);
            Vertecies[1] = new Vertex(new Vector2f(SideLength, 0), Color);

            target.Draw(Vertecies, states);
        }

        public override string ToString()
        {
            return $"({RelativePosition.X},{RelativePosition.Y}) - {SideLength}";
        }
    }
}
