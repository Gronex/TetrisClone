using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace TetrisClone.Models
{
    public class GameBoard : Drawable
    {
        private readonly Texture _texture;
        private readonly int _height;
        private readonly int _width;

        private readonly Block[][] _gameBoard;
        

        public GameBoard(int height, int width)
        {
            _texture = new Texture("Resources/Board.png");
            _height = height;
            _width = width;
            _gameBoard = new Block[height][];
            for (var i = 0; i < _gameBoard.Length; i++)
            {
                _gameBoard[i] = new Block[width];
            }
        }

        public void Place(Tetramino tetramino)
        {
            foreach (var block in tetramino.Blocks)
            {
                if(0 > block.RelativePosition.X || _width <= block.RelativePosition.X ||
                    0 > block.RelativePosition.Y || _height <= block.RelativePosition.Y) continue;
                _gameBoard[(int) block.RelativePosition.Y][(int) block.RelativePosition.X] = block;
            }
        }

        public bool IsTaken(int x, int y)
        {
            return !(y >= 0 && y < _height &&
                x >= 0 && x < _width && 
                _gameBoard[y][x] == null);
        }

        public int ClearCheck()
        {
            var cleared = 0;
            for (var i = 0; i < _gameBoard.Length; i++)
            {
                if (_gameBoard[i].Any(b => b == null)) continue;

                cleared++;
                // Clear line
                _gameBoard[i] = _gameBoard[i].Select<Block, Block>(b => null).ToArray();

                //Move above one down everything down
                for (var j = i; j >= 0; j--)
                {
                    if(j == 0)
                        _gameBoard[j] = new Block[_width];
                    else
                        _gameBoard[j] = _gameBoard[j - 1];
                    foreach (var block in _gameBoard[j].Where(b => b != null))
                    {
                        block.Move(new Vector2f(0, 1));
                    }
                }
            }
            return cleared;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            //TODO: make propper
            var sprite = new Sprite(_texture);
            target.Draw(sprite);


            foreach (var row in _gameBoard)
            {
                foreach (var block in row)
                {
                    block?.Draw(target, states);
                }
            }
        }
    }
}
