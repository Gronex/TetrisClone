using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using TetrisClone.Utils;

namespace TetrisClone.Models
{
    public enum TetraminoShape
    {
        T,
        S,
        Z,
        I,
        L,
        O
    }

    public class Tetramino : Drawable
    {
        private static readonly Random Random = new Random();

        private readonly Block[] _blocks;
        private Vector2f _center;
        public int BlockSideLength { get; }
        public TetraminoShape Shape { get; }

        public Tetramino(TetraminoShape shape, int blockSideLength)
        {
            BlockSideLength = blockSideLength;

            _center = new Vector2f(BlockSideLength,BlockSideLength);
            Shape = shape;
            switch (shape)
            {
                case TetraminoShape.T:
                    _blocks = new[]
                    {
                        new Block(blockSideLength, Color.Yellow, new Vector2f(0,0)),
                        new Block(blockSideLength, Color.Yellow, new Vector2f(1,0)),
                        new Block(blockSideLength, Color.Yellow, new Vector2f(2,0)),
                        new Block(blockSideLength, Color.Yellow, new Vector2f(1,1))
                    };
                    break;
                case TetraminoShape.S:
                    _blocks = new []
                    {
                        new Block(blockSideLength, Color.Green, new Vector2f(0,0)),
                        new Block(blockSideLength, Color.Green, new Vector2f(0,1)),
                        new Block(blockSideLength, Color.Green, new Vector2f(1,1)),
                        new Block(blockSideLength, Color.Green, new Vector2f(1,2))
                    };
                    break;
                case TetraminoShape.Z:
                    _blocks = new []
                    {
                        new Block(blockSideLength, Color.Blue, new Vector2f(1,0)),
                        new Block(blockSideLength, Color.Blue, new Vector2f(1,1)),
                        new Block(blockSideLength, Color.Blue, new Vector2f(0,1)),
                        new Block(blockSideLength, Color.Blue, new Vector2f(0,2))
                    };
                    break;
                case TetraminoShape.I:
                    _blocks = new []
                    {
                        new Block(blockSideLength, Color.Cyan, new Vector2f(0,0)),
                        new Block(blockSideLength, Color.Cyan, new Vector2f(1,0)),
                        new Block(blockSideLength, Color.Cyan, new Vector2f(2,0)),
                        new Block(blockSideLength, Color.Cyan, new Vector2f(3,0))
                    };
                    break;
                case TetraminoShape.L:
                    _blocks = new []
                    {
                        new Block(blockSideLength, new Color(255, 165,0), new Vector2f(0,0)),
                        new Block(blockSideLength, new Color(255, 165,0), new Vector2f(1,0)),
                        new Block(blockSideLength, new Color(255, 165,0), new Vector2f(2,0)),
                        new Block(blockSideLength, new Color(255, 165,0), new Vector2f(2,1))
                    };
                    break;
                case TetraminoShape.O:
                    _blocks = new []
                    {
                        new Block(blockSideLength, new Color(128, 0, 128), new Vector2f(0,0)),
                        new Block(blockSideLength, new Color(128, 0, 128), new Vector2f(1,0)),
                        new Block(blockSideLength, new Color(128, 0, 128), new Vector2f(1,1)),
                        new Block(blockSideLength, new Color(128, 0, 128), new Vector2f(0,1))
                    };
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
            }
            Console.WriteLine(this);
        }

        public IEnumerable<Block> Blocks => _blocks;

        public void Move(Vector2f direction)
        {
            _center += direction*BlockSideLength;
            foreach (var block in _blocks)
            {
                block.Move(direction);
            }
        }

        public void Rotate(int degrees)
        {
            var rad = degrees.ToRadian();
            foreach (var block in _blocks)
            {
                //Reset coords to be around 0,0
                var relativePos = block.Position - _center;
                //Rotate
                var newRelative = new Vector2f(
                    (float)(relativePos.X * Math.Cos(rad) - relativePos.Y * Math.Sin(rad)),
                    (float)(relativePos.X * Math.Sin(rad) - relativePos.Y * Math.Cos(rad))
                    );
                //Set coords back
                block.Position = newRelative + _center;
            }
        }
        
        public void Draw(RenderTarget target, RenderStates states)
        {
            foreach (var block in _blocks)
            {
                target.Draw(block, states);
            }
        }

        public override string ToString()
        {
            return $"{Shape}: [{string.Join(",\n", _blocks.AsEnumerable())}]";

        }
        
        public static Tetramino RandomTetramino(int blockSideLength)
        {
            var values = Enum.GetValues(typeof(TetraminoShape));
            var shape = (TetraminoShape)values.GetValue(Random.Next(values.Length));
            return new Tetramino(shape, blockSideLength);
        }

    }
}
